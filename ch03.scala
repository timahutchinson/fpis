object List {
  // Exercise 3.2
  def tail[A](as: List[A]): List[A] = as match {
    case Nil => Nil
    case _ :: a => a
  }


  // Exercise 3.3
  def setHead[A](as: List[A], b: A): List[A] = as match {
    case Nil => Nil
    case _ :: a => b :: a
  }


  // Exercise 3.4
  def drop[A](as: List[A], n: Int): List[A] = as match {
    case Nil => Nil
    case _ :: a => if (n == 1) a else drop(a, n-1)
  }


  // Exercise 3.5
  def dropWhile[A](as: List[A], f: A => Boolean): List[A] = as match {
    case Nil => Nil
    case a :: b => if (f(a)) dropWhile(b, f) else a :: b
  }


  // Exercise 3.6
  def init[A](as: List[A]): List[A] = as match {
    case Nil => Nil
    case a :: Nil => Nil
    case a :: b => a :: init(b)
  }


  // Exercise 3.9
  def foldRight[A,B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case x :: xs => f(x, foldRight(xs, z)(f))
  }

  def length[A](as: List[A]): Int = {
    foldRight(as, 0)((x, y) => 1 + y)
  }


  // Exercise 3.10
  @annotation.tailrec
  def foldLeft[A,B](as: List[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case x :: xs => foldLeft(xs, f(z,x))(f)
  }


  // Exercise 3.11
  def sumLeft(as: List[Int]): Int = {
    foldLeft(as, 0)((x, y) => x + y)
  }

  def productLeft(as: List[Int]): Int = {
    foldLeft(as, 1)((x, y) => x * y)
  }

  def lengthLeft[A](as: List[A]): Int = {
    foldLeft(as, 0)((x, _) => x + 1)
  }


  // Exercise 3.12
  def reverse[A](as: List[A]): List[A] = {
    foldLeft(as, List[A]())((acc, h) => h :: acc)
  }
}
