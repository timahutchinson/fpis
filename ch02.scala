// Exercise 2.1
def fibonacci(n: Int): Int = {
  @annotation.tailrec
  def loop(a: Int, b: Int, count: Int): Int = {
    if (count == n) b
    else loop(a+b, a, count+1)
  }
  loop(1, 0, 1)
}


// Exercise 2.2
def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
  @annotation.tailrec
  def loop(n: Int): Boolean = {
    if (n >= as.length) true
    else if (!(ordered(as(n), as(n-1)))) false
    else loop(n+1)
  }
  loop(1)
}


// Exercise 2.3
def curry[A,B,C](f: (A, B) => C): A => (B => C) =
  (a: A) => (b: B) => f(a, b)


// Exercise 2.4
def uncurry[A,B,C](f: A => B => C): (A, B) => C =
  (a: A, b: B) => f(a)(b)


// Exercise 2.5
def compose[A,B,C](f: B => C, g: A => B): A => C =
  (a: A) => f(g(a))
